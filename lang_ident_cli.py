#! /usr/bin/env python3

import langid
import fasttext
import sys
sys.path.extend(["ensemble", "custom_fasttext_wrapper", "dict_lang_ident"])

from lang_ident_ensemble import LangIdentEnsemble


def main():
    ensemble = LangIdentEnsemble(("dict_lang_ident/dictionaries", "dict_lang_ident/bigram_dicts"))
    while True:
        try:
            sentence = input(">>>")
            print("Voting result:", ensemble.identify_language(sentence, debug=True))
        except (KeyboardInterrupt, EOFError):
            break
    

if __name__ == "__main__":
    main()
