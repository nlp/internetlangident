MODELS=models/lang.bin models/lid.176.bin
DATASETS=officebot_fasttext_dataset.txt

.PHONY: models all clean fasttext_train

all: $(MODELS) $(DATASETS)

models: $(MODELS)

models/lang.bin:
	wget https://nlp.fi.muni.cz/projekty/officebot/lang.bin -O $@


models/lid.176.bin:
	wget https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.bin -O $@


officebot_fasttext_dataset.txt:
	wget https://nlp.fi.muni.cz/projekty/officebot/fasttext_train.txt -O $@

clean:
	$(RM) $(MODELS) $(DATASETS)

fasttext_train:
	fasttext supervised -input officebot_fasttext_dataset.txt -output langdetect -dim 16 -minn 2 -maxn 4 -loss hs
